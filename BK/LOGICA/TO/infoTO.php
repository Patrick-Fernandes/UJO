<?php

class infoTO {

    private $CODINFO;
    private $ENDERECO;
    private $hora_inicio;
    private $hora_fim;
    private $cod_postagem;
    private $extras;

    public function __construct() {
        
    }

    //SET
    public function setId($id) {
        $this->CODINFO = $id;
    }

    public function setENDERECO($end) {
        $this->ENDERECO = $end;
    }

    public function setHORAinc($HORAINC) {
        $this->hora_inicio = $HORAINC;
    }

    public function setHORAfim($horafim) {
        $this->hora_fim = $horafim;
    }

    public function setCODPOSTAGEM($idPOST) {
        $this->cod_postagem = $idPOST;
    }

    public function seteXTRAS($eXTRAS) {
        $this->extras = $eXTRAS;
    }

    //GET

    public function getId() {
        return $this->CODINFO;
    }

    public function getENDERECO() {
        return $this->ENDERECO;
    }

    public function getHORAinc() {
        return $this->hora_inicio;
    }

    public function getHORAfim() {
        return $this->hora_fim;
    }

    public function getCODPOSTAGEM() {
        return $this->cod_postagem;
    }

    public function geteXTRAS() {
        return $this->extras;
    }

}
