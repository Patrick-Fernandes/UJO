<?php

function limita_caracteres($texto, $limite, $quebra = true) {
    $tamanho = strlen($texto);
    if ($tamanho <= $limite) {
        $novo_texto = $texto;
    } else {
        if ($quebra == true) {
            $novo_texto = trim(substr($texto, 0, $limite)) . "...<br>";
        } else {
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " <br>");
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco)) . "...<br>";
        }
    } return $novo_texto;
}
function titulo12($str) {
    $str = str_replace("teste_slider", "", $str);
    $str = str_replace("N-", "", $str);
    $str = str_replace("/", "", $str);
    $str = str_replace("|", "", $str);
    $str = str_replace("\\", "", $str);
    $str = str_replace("PAG", "", $str);
    $str = str_replace("-", " ", $str);
    $str = str_replace("_", " ", $str);
    return $str;
}

function espaco($str) {
    $str = str_replace(" ", "-", $str);
    $str = str_replace("'", "\"", $str);
    $str = str_replace("img", "IMG", $str);
    return $str;
}
function ASPA($str) {
    $str = str_replace("'", "\"", $str);
    $str = str_replace("img", "IMG", $str);
    return $str;
}
//securyti
function XSS($str) {
    $str = str_replace("<", "", $str);
    $str = str_replace(">", "", $str);
    $str = str_replace("'", "\'", $str);
    return $str;
}

function arrumaLink($text) {

   $text = preg_replace('|(https?://([\d\w\.-]+\.[\w\.]{2,6})[^\s\]\[\<\>]*/?)|i', '<a href="$1">$2</a>', $text);
$text = preg_replace('|\B#([\d\w_]+)|i', '<a href="http://url.com/pluslink/$1">$0</a>', $text);
$text = preg_replace('|\B\+([\d\w_]+)|i', '<a href="http://url.com/pluslink/$1">$0</a>', $text);
	return $text;
}
function XSS2($str2) {

    $str2 = str_replace("/teste_slider", "", $str2);
    $str2 = str_replace("?pagina=1", "", $str2);
    $str2 = str_replace("?pagina=2", "", $str2);
    $str2 = str_replace("?pagina=3", "", $str2);
    $str2 = str_replace("?pagina=4", "", $str2);
    $str2 = str_replace("?pagina=5", "", $str2);
    $str2 = str_replace("?pagina=6", "", $str2);
    $str2 = str_replace("?pagina=7", "", $str2);
    $str2 = str_replace("?pagina=8", "", $str2);
    $str2 = str_replace("?pagina=9", "", $str2);
    $str2 = str_replace("?pagina=10", "", $str2);
    $str2 = str_replace("?pagina=11", "", $str2);
    $str2 = str_replace("?pagina=12", "", $str2);
    $str2 = str_replace("?pagina=13", "", $str2);

    $str2 = str_replace("--PAG-2", "", $str2);
    $str2 = str_replace("--PAG-3", "", $str2);
    $str2 = str_replace("--PAG-1", "", $str2);
    $str2 = str_replace("--PAG-4", "", $str2);
    $str2 = str_replace("--PAG-5", "", $str2);
    $str2 = str_replace("--PAG-6", "", $str2);
    $str2 = str_replace("--PAG-7", "", $str2);
    $str2 = str_replace("--PAG-8", "", $str2);
    $str2 = str_replace("--PAG-9", "", $str2);
    $str2 = str_replace("--PAG-10", "", $str2);
    $str2 = str_replace("--PAG-11", "", $str2);
    $str2 = str_replace("--PAG-12", "", $str2);

    $str2 = str_replace("-PAGINA-1", "", $str2);
    $str2 = str_replace("-PAGINA-2", "", $str2);
    $str2 = str_replace("-PAGINA-3", "", $str2);
    $str2 = str_replace("-PAGINA-4", "", $str2);
    $str2 = str_replace("-PAGINA-5", "", $str2);
    $str2 = str_replace("-PAGINA-6", "", $str2);
    $str2 = str_replace("-PAGINA-7", "", $str2);
    $str2 = str_replace("-PAGINA-8", "", $str2);
    $str2 = str_replace("-PAGINA-9", "", $str2);
    $str2 = str_replace("-PAGINA-10", "", $str2);
    $str2 = str_replace("-PAGINA-11", "", $str2);
    $str2 = str_replace("-PAGINA-12", "", $str2);

    $str2 = str_replace("?npag=1", "", $str2);
    $str2 = str_replace("?npag=2", "", $str2);
    $str2 = str_replace("?npag=3", "", $str2);
    $str2 = str_replace("?npag=4", "", $str2);
    $str2 = str_replace("?npag=5", "", $str2);
    $str2 = str_replace("?npag=6", "", $str2);
    $str2 = str_replace("?npag=7", "", $str2);
    $str2 = str_replace("?npag=8", "", $str2);
    $str2 = str_replace("?npag=9", "", $str2);
    $str2 = str_replace("?npag=10", "", $str2);
    $str2 = str_replace("?npag=11", "", $str2);
    $str2 = str_replace("?npag=12", "", $str2);
    $str2 = str_replace("?npag=13", "", $str2);
    $str2 = str_replace("?npag=15", "", $str2);
    $str2 = str_replace("?npag=14", "", $str2);
    $str2 = str_replace("?npag=16", "", $str2);
    $str2 = str_replace("?npag=18", "", $str2);
    $str2 = str_replace("?npag=17", "", $str2);
    $str2 = str_replace("?npag=19", "", $str2);
    $str2 = str_replace("?npag=20", "", $str2);
    $str2 = str_replace("?npag=21", "", $str2);
    $str2 = str_replace("?npag=22", "", $str2);
    $str2 = str_replace("?npag=23", "", $str2);
    $str2 = str_replace("?npag=24", "", $str2);
    $str2 = str_replace("?npag=25", "", $str2);
    return $str2;
}

function limpaUrl($str2) { 
    
    switch ($str2) {
        //INDES
        case "/aulaPI2/teste/index.php":
            echo "<script>     function navegador() {   window.location = 'HOME';}     navegador();    </script>";
            break;
//AGENDA
        case "/aulaPI2/teste/AGENDA.php" :
            echo "<script>     function navegador() {   window.location = 'AGENDA';}     navegador();    </script>";
            break;
        //parceiros
        case "/aulaPI2/teste/PARCEIROS.php":
            echo "<script>     function navegador() {   window.location = 'PARCEIROS';}     navegador();    </script>";
            break;
        case "/aulaPI2/teste/404.php": 
            echo "<script>     function navegador() {   window.location = '403';}     navegador();    </script>";
            break;
           case "/aulaPI2/teste/403.php": 
            echo "<script>     function navegador() {   window.location = '403';}     navegador();    </script>";
            break;
    }
}
